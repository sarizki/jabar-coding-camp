// soal 1

// substring(indexAwal, indexAkhir)
// substr(indexAwal, jumlahKarakter)

var pertama = 'saya sangat senang hari ini'
var kedua = 'belajar javascript itu keren'

// output = saya senang belajar JAVASCRIPT
var Kata1 = pertama.substr(0, 4)
var Kata2 = pertama.substring(12, 18)
var Kata3 = kedua.substring(0, 7)
var Kata4 = kedua.substring(8, 18).toUpperCase()
var output1 = Kata1 + ' ' + Kata2 + ' ' + Kata3 + ' ' + Kata4
// console.log(output1)

// soal 2
var kataPertama = '10'
var kataKedua = '2'
var kataKetiga = '4'
var kataKeempat = '6'

var number1 = Number(kataPertama)
var number2 = Number(kataKedua)
var number3 = Number(kataKetiga)
var number4 = Number(kataKeempat)

var output2_1 = number1 + number2 * number3 + number4
var output2_2 = number2 * number3 + (number1 + number4)
var output2_3 = (number1 % number2) + number3 * number4
var output2_4 = number1 + number4 + number2 * number3

// soal 3
var kalimat = 'wah javascript itu keren sekali'

var kataPertama = kalimat.substring(0, 3)
var kataKedua = kalimat.substring(4, 14)
var kataKetiga = kalimat.substring(15, 18)
var kataKeempat = kalimat.substring(19, 24)
var kataKelima = kalimat.substring(25)

console.log('Kata Pertama: ' + kataPertama)
console.log('Kata Kedua: ' + kataKedua)
console.log('Kata Ketiga: ' + kataKetiga)
console.log('Kata Keempat: ' + kataKeempat)
console.log('Kata Kelima: ' + kataKelima)
