//conditional & loop

// looping

//while loop
// while ([Kondisi]) {
//   // Kondisi yang menentukan apakah program akan melakukan iterasi.
//   // Berupa boolean atau true/false.
//   [Proses] // Merupakan proses yang akan dijalankan dalam satu iterasi
// }

// var deret = 5
// var jumlah = 0

// while (deret > 0) {
//   jumlah += deret
//   //   jumlah = jumlah + deret
//   console.log('jumlah: ', jumlah)
// }

// console.log('hasil akhir jumlah : ', jumlah)

// for loop
// for([Inisialisasi], [Kondisi], [Incremental/Decremental]) {
//     [Proses] // Merupakan proses yang akan dijalankan dalam satu iterasi
//   }

// var jumlah = ''
// for (var index = 5; index > 0; index--) {
//   jumlah += '\n'
//   //enter
//   jumlah += 'halo'
// }

// console.log(jumlah)